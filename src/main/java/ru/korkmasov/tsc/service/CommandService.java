package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.ICommandService;
import ru.korkmasov.tsc.api.ICommandRepository;
import ru.korkmasov.tsc.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
