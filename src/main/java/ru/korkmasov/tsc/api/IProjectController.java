package ru.korkmasov.tsc.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
