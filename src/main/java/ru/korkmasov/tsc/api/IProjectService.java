package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();
}
