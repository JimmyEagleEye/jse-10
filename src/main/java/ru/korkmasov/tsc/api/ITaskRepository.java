package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();
}
