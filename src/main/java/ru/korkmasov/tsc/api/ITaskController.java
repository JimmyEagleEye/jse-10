package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    Task add(String name, String description);
}
